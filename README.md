# Kube Airflow

## Quick start

Create all the persistent volume directory within glusterfs.
```bash
gluster volume create airflow replica 4 192.168.0.2{1..4}:/mnt/usb1/airflow
mkdir /tmp/airflow
mount -t glusterfs localhost:/airflow /tmp/airflow
mkdir -p /tmp/airflow/{dags,logs,pg,plugins}
rmdir /tmp/airflow
```

Airflow Docker image need to be built on ARM64 system before running kubectl.
```bash
docker build -t yueyehua/airflow:$(grep "ARG AIRFLOW_VERSION=" Dockerfile|cut -d'"' -f2) .
```

After starting postgres, you may need to create manually the airflow database.
U may
```bash
kubectl -n airflow exec -ti pod/postgres-<random_hash> bash
psql -U airflow -c 'CREATE DATABASE airflow;'
grep "host	all		all	all		trust" /var/lib/postgresql/data/pg_hba.conf || \
  echo "host	all		all	all		trust" >> /var/lib/postgresql/data/pg_hba.conf
exit
```

```bash
kubectl apply -f airflow-namespace.yaml
kubectl apply -f airflow-endpoint.yaml
kubectl apply -f postgres-data.yaml
kubectl apply -f postgres-config.yaml
kubectl apply -f postgres-deployment.yaml
kubectl apply -f postgres-service.yaml
kubectl apply -f airflow-data.yaml
kubectl apply -f airflow-config.yaml
kubectl apply -f airflow-podtemplate.yaml
kubectl apply -f airflow-rbac.yaml
kubectl apply -f airflow-scheduler-deployment.yaml
kubectl apply -f airflow-webserver-deployment.yaml
kubectl apply -f airflow-webserver-service.yaml
```

## Removal

```bash
kubectl delete -f airflow-webserver-service.yaml
kubectl delete -f airflow-webserver-deployment.yaml
kubectl delete -f airflow-scheduler-deployment.yaml
kubectl delete -f airflow-rbac.yaml
kubectl delete -f airflow-podtemplate.yaml
kubectl delete -f airflow-config.yaml
kubectl delete -f airflow-data.yaml
kubectl delete -f postgres-service.yaml
kubectl delete -f postgres-deployment.yaml
kubectl delete -f postgres-config.yaml
kubectl delete -f postgres-data.yaml
kubectl delete -f airflow-endpoint.yaml
kubectl delete -f airflow-namespace.yaml
```
